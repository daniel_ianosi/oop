<div class="sidebar-item-holder">
    <h5 class=" font-weight-5 sp-sb-title">Search</h5>
    <input type="text" name="name" class="sp-news-letter" value="Search" maxlength="100" />
</div>
<!--end item holder-->

<div class="sidebar-item-holder">
    <h5 class=" font-weight-5 sp-sb-title">Categories</h5>
    <ul class="sp-sb-links">
        <?php $categories = Category::findBy(['1'=>'1']); ?>
        <?php foreach ($categories as $category):?>
            <li><a href="category.php?id=<?php echo $category->id; ?>"><?php echo $category->name; ?> (<?php echo count($category->getProducts()); ?>)</a></li>
        <?php endforeach; ?>
    </ul>
</div>
<!--



<div class="sidebar-item-holder">
    <h5 class=" font-weight-5 sp-sb-title">Featured Products</h5>
    <div class="col-md-12 col-sm-12 col-xs-12 nopadding">
        <div class="imgbox-small left"> <img src="http://via.placeholder.com/80x80" alt="" class="img-responsive"/></div>
        <div class="text-box-right">
            <h6 class=" nopadding"><a href="#" class="text-hover-gyellow">Casual Shoes</a></h6>
            <div class="blog-post-info padding-top-1"> <span> By Benjamin</span> <span> 15 Comments</span> </div>
        </div>
    </div>


    <div class="divider-line solid light margin"></div>
    <div class="col-md-12 col-sm-12 col-xs-12 nopadding">
        <div class="imgbox-small left"> <img src="http://via.placeholder.com/80x80" alt="" class="img-responsive"/></div>
        <div class="text-box-right">
            <h6 class=" nopadding"><a href="#" class="text-hover-gyellow">Men's Casuals</a></h6>
            <div class="blog-post-info padding-top-1"> <span> By Benjamin</span> <span> 15 Comments</span> </div>
        </div>
    </div>


    <div class="divider-line solid light margin"></div>
    <div class="col-md-12 col-sm-12 col-xs-12 nopadding">
        <div class="imgbox-small left"> <img src="http://via.placeholder.com/80x80" alt="" class="img-responsive"/></div>
        <div class="text-box-right">
            <h6 class=" nopadding"><a href="#" class="text-hover-gyellow">Watches</a></h6>
            <div class="blog-post-info padding-top-1"> <span> By Benjamin</span> <span> 15 Comments</span> </div>
        </div>
    </div>

</div>




<div class="bg2-right-col-item-holder">
    <h5 class=" font-weight-5 sp-sb-title">Tags</h5>
    <ul class="sp-tags">
        <li><a href="#">Fashion</a></li>
        <li><a href="#">Casuals</a></li>
        <li><a href="#">Dresses</a></li>
        <li><a href="#">Offers</a></li>
        <li><a class="active" href="#">tops</a></li>
        <li><a href="#">Art</a></li>
        <li><a href="#">Shoes</a></li>
        <li><a href="#">Watches</a></li>
    </ul>
</div>
-->