<?php


class User extends BaseTable
{
    public $email;
    public $password;
    public $status;

    public static function getTable()
    {
        return 'users';
    }

    public function getAddresses()
    {
        return Address::findBy(['user_id'=>$this->id]);
    }

    public function getFavorites()
    {
        return Favorite::findBy(['user_id'=>$this->id]);
    }

    public function getCart()
    {
        $cart = Cart::findOneBy(['user_id'=>$this->id]);
        if (!$cart){
            $cart = new Cart(['user_id'=>$this->id]);
            $cart->save();
        }

        return $cart;
    }

}