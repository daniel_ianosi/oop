<?php


class Cart extends BaseTable
{
    public $user_id;
    public $address_id;

    static function getTable()
    {
        return 'carts';
    }

    public function getCartItems()
    {
        return CartItem::findBy(['cart_id'=>$this->id]);
    }

    public function getPrice()
    {
        $price = 0;
        foreach ($this->getCartItems() as $cartItem){
            $price+=$cartItem->getPrice();
        }

        return $price;
    }
}