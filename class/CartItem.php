<?php


class CartItem extends BaseTable
{
    public $product_id;
    public $quantity;
    public $cart_id;

    static function getTable()
    {
        return 'cart_items';
    }

    public function getProduct()
    {
        return Product::find($this->product_id);
    }

    public function getPrice()
    {
        return $this->getProduct()->getFinalPrice() * $this->quantity;
    }
}