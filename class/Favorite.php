<?php


class Favorite extends BaseTable
{
    public $user_id;
    public $product_id;


    public static function getTable()
    {
        return 'favorites';
    }

    public function getProduct()
    {
        return Product::find($this->product_id);
    }
}