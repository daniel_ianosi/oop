<?php


class Category extends BaseTable
{
    public $name;


    public static function getTable()
    {
        return 'categories';
    }

    public function getProducts()
    {
        return Product::findBy(['category'=>$this->name]);
    }

}