<?php


class Product extends BaseTable
{
    public $name;

    public $picture;

    public $oldPrice;

    public $discount;

    public $stoc;

    public $vendor;

    /**
     * @return mixed
     */
    public function getOldPrice()
    {
        return $this->oldPrice;
    }




    public function getFinalPrice()
    {
        return ceil($this->oldPrice*((100-$this->discount)/100))-0.01;
    }

    /**
     * @return Review[]
     */
    public function getReviews()
    {
        return Review::findBy(['product_id'=>$this->id]);
    }

    /**
     * @return Picture[]
     */
    public function getPictures()
    {
        return Picture::findBy(['product_id'=>$this->id]);
    }

    public static function getTable()
    {
        return 'products';
    }

}


